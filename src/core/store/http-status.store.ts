// core/store/http-status.store.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../App';

interface HttpStatus {
  status: 'error' | 'success' | null,
  actionType: string | null
}

export const httpStatusStore = createSlice({
  name: 'httpStatus',
  initialState: { status: null, actionType: null } as HttpStatus,
  reducers: {
    setHttpStatus(state, action: PayloadAction<HttpStatus>) {
      state.status = action.payload.status;
      state.actionType = action.payload.actionType;
    },
  },
  extraReducers: {
    'products/add/rejected'  : (state, action) => {
      state.status = 'error';
      state.actionType = action.payload
    },
  }
})

export const {
  setHttpStatus
} = httpStatusStore.actions;


// selector
export const selectHttpStatus = (state: RootState) => state.httpStatus;
