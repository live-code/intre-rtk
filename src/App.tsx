import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { CounterPage } from './pages/counter/CounterPage';
import { UsersPage } from './pages/users/UsersPage';
import { CatalogPage } from './pages/catalog/CatalogPage';
import { HomePage } from './pages/home/HomePage';
import { Navbar } from './core/components/Navbar';
import { Action, combineReducers, configureStore, getDefaultMiddleware, ThunkAction } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { counterReducer } from './pages/counter/store/counter/counter.reducer';
import { configReducer } from './pages/counter/store/config/config.reducer';
import { warehouseReducer } from './pages/counter/store';
import { newsStore } from './pages/home/store/news/news.store';
import { newsFiltersStore } from './pages/home/store/filters/news-filters.store';
import { homeReducer } from './pages/home/store';
import { productsStore } from './pages/catalog/store/products/products.store';
import { categoriesStore } from './pages/catalog/store/categories/categories.store';
import { catalogReducer } from './pages/catalog/store';
import { httpStatusStore } from './core/store/http-status.store';

const rootReducer = combineReducers({
  warehouse: warehouseReducer,
  home: homeReducer,
  catalog: catalogReducer,
  httpStatus: httpStatusStore.reducer
})

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;


export const store = configureStore({
  reducer: rootReducer,
  devTools:
    process.env.NODE_ENV === 'production' ?
      false :
      {
        maxAge: 10
      },

})
export type AppDispatch = typeof store.dispatch;


function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route path="/counter">
            <CounterPage />
          </Route>
          <Route path="/users">
            <UsersPage />
          </Route>
          <Route path="/catalog">
            <CatalogPage />
          </Route>
          <Route path="/" exact>
            <HomePage />
          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
