export interface News {
  id: number | string;
  title: string;
  published: boolean;
}
