import { combineReducers } from '@reduxjs/toolkit';
import { newsStore } from './news/news.store';
import { newsFiltersStore } from './filters/news-filters.store';

export const homeReducer = combineReducers({
  news: newsStore.reducer,
  newsFilters: newsFiltersStore.reducer
})
