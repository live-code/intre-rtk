import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { News } from '../../model/news';

export interface Filters {
  visibility: string;
  color: string;
}

export const newsFiltersStore = createSlice({
  name: 'users',
  initialState: { visibility: 'all', color: 'red' },
  reducers: {
    changeFilters(state, action: PayloadAction<Partial<Filters>>) {
      return { ...state, ...action.payload }
    }
  }
})

export const { changeFilters } = newsFiltersStore.actions;

