import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { News } from '../../model/news';

export const newsStore = createSlice({
  name: 'users',
  initialState: [] as News[],
  reducers: {
    addNews(state, action: PayloadAction<string>) {
      state.push({ title: action.payload, published: false, id: action.payload })
    },
    deleteNews(state, action: PayloadAction<number | string>) {
      const index = state.findIndex(news => news.id === action.payload)
      state.splice(index, 1);
      // return state.filter(news => news.id !== action.payload)
    },
    toggleNews(state, action: PayloadAction<number | string>) {
      // const index = state.findIndex(news => news.id === action.payload)
      // state[index].published = !state[index].published
      const news = state.find(news => news.id === action.payload);
      if (news) {
        news.published = !news.published;
      }
    },
  },
})

export const { addNews, deleteNews, toggleNews } = newsStore.actions;
