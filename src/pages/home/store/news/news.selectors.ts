import { RootState } from '../../../../App';

export const getNews = (state: RootState) => {
    switch(state.home.newsFilters.visibility) {
      case 'published': return state.home.news.filter(n => n.published)
      case 'unpublished': return state.home.news.filter(n => !n.published)
      default: return state.home.news;
    }
  }
