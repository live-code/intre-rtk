import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addNews, deleteNews, toggleNews } from './store/news/news.store';
import { RootState } from '../../App';
import { getNews } from './store/news/news.selectors';
import { News } from './model/news';
import { changeFilters } from './store/filters/news-filters.store';

function addNew() {
  // axios.post....

}

export const HomePage: React.FC = () => {
  const newsList: News[] = useSelector(getNews);
  const visibilityFilter = useSelector((state: RootState) => state.home.newsFilters.visibility)
  const dispatch = useDispatch();

  function onChange(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
      e.currentTarget.value = '';
    }
  }

  return <div>
    <input type="text" onKeyPress={onChange} placeholder="add news"/>
    <select
      onChange={
      (e) => dispatch(changeFilters({ visibility: e.currentTarget.value }))
    }>
      <option selected={visibilityFilter === 'all'} value="all">Show all</option>
      <option selected={visibilityFilter === 'published'}  value="published">Published only</option>
      <option selected={visibilityFilter === 'unpublished'} value="unpublished">Unpublished only</option>
    </select>

    <hr/>
    {
      newsList.map(news => {
        return <li key={news.id}>
          {news.title}
          <i
            className="fa fa-trash"
            onClick={() => dispatch(deleteNews(news.id))}
          />

          <div
            className="badge badge-dark"
            onClick={() => dispatch(toggleNews(news.id))}
          >
            {news.published ? 'PUBLISHED' : 'UNPUBLISHED'}
          </div>
        </li>
      })
    }
  </div>
};
