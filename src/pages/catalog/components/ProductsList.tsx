import { deleteProduct, toggleProduct } from '../store/products/products.actions';
import cn from 'classnames';
import React from 'react';
import { Product } from '../model/product';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../App';

interface ProductsListProps {
  products: Product[];
  onDelete: (id: number) => void;
  onToggle: (id: number, visibility: boolean) => void;
}
export const ProductsList: React.VFC<ProductsListProps> = React.memo((props) => {
  console.log('render: list')
  return (<ul>
    {
      props.products.map(p => {
        return <li key={p.id}>
          {p.title}
          <i className="fa fa-trash"
             onClick={() => props.onDelete(p.id)} />

          <i
            onClick={() => props.onToggle(p.id, !p.visibility)}
            className={cn(
              'fa',
              { 'fa-eye': p.visibility},
              { 'fa-eye-slash': !p.visibility},
            )}
          />
        </li>
      })
    }
  </ul>)
})
