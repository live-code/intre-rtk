import React, { useState } from 'react';
import { addProduct } from '../store/products/products.actions';
import { Product } from '../model/product';
import classnames from 'classnames';

type ProdPartial = Pick<Product, 'title' | 'price' | 'categoryId'>;
const initialState = { title: '', price: 0, categoryId: -1};

interface ProductsFormProps {
  categories: { id: number, name: string }[];
  onSubmit: (product: Partial<Product>) => void;
}
export const ProductsForm: React.FC<ProductsFormProps> = props => {
  const [ formData, setFormData ] = useState<ProdPartial>(initialState);
  const [ dirty, setDirty ] = useState<boolean>(false);

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    setDirty(true);
    const type = e.currentTarget.type;
    const value = e.currentTarget.value;
    setFormData({
      ...formData,
      [e.currentTarget.name]: type === 'number' || type == 'select-one' ? +value : value
    })
  }

  function submitHandler(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    props.onSubmit(formData);
    setFormData(initialState)
  }

  const isTitleValid = formData.title !== '';
  const isPriceValid = formData.price > 0;
  const isCategoryValid = formData.categoryId !== -1
  const valid =  isTitleValid &&  isPriceValid && isCategoryValid;

  return (
    <form onSubmit={submitHandler}>
      <input
        className={classnames('form-control', { 'is-invalid': !isTitleValid && dirty})}
        type="text"
        value={formData.title} onChange={onChangeHandler} name="title"/>

      <input
        className={classnames('form-control', { 'is-invalid': !isPriceValid && dirty})}
        type="number" value={formData.price} onChange={onChangeHandler} name="price"/>

      <select
        className={classnames('form-control', { 'is-invalid': !isCategoryValid && dirty})}
        name="categoryId" value={formData.categoryId} onChange={onChangeHandler} >
        <option value={-1}>Select a Category</option>
        {
          props.categories.map(c => {
            return <option key={c.id} value={c.id}>{c.name}</option>
          })
        }
      </select>

      <button type="submit" disabled={!valid}>ADD PRODUCT</button>

    </form>
  )
}
