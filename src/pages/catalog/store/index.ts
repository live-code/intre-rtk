import { combineReducers } from '@reduxjs/toolkit';
import { productsStore } from './products/products.store';
import { categoriesStore } from './categories/categories.store';

export const catalogReducer = combineReducers({
  list: productsStore.reducer,
  categories: categoriesStore.reducer,
})
