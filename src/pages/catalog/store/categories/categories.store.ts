import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export const categoriesStore = createSlice({
  name: 'categories',
  initialState: {
    list: [
      { id: 1, name: 'Latticini' },
      { id: 2, name: 'Frutta' },
      { id: 3, name: 'Cioccolato' },
    ],
    active: -1
  },
  reducers: {
    changeActiveCategory(state, action: PayloadAction<number>) {
      state.active = action.payload;
    }
  }
})

export const { changeActiveCategory } = categoriesStore.actions
