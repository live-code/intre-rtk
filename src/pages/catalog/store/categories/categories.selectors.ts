import { RootState } from '../../../../App';

export const getCategories = (state: RootState) => state.catalog.categories.list
export const getActiveCategory = (state: RootState) => state.catalog.categories.active
