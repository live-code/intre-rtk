import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../../model/product';

export const productsStore = createSlice({
  name: 'products',
  initialState: {
    data: [] as Product[],
    error: false
  },
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      return {
        error: false,
        data: action.payload
      }
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.data.push(action.payload)
      state.error = false;
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      const index = state.data.findIndex(p => p.id === action.payload);
      state.data.splice(index, 1);
      state.error = false;
    },
    deleteProductFail(state) {
      state.error = true;
    },
    toggleProductVisibilitySuccess(state, action: PayloadAction<Product>) {
      const product = state.data.find(p => p.id === action.payload.id)
      if (product) {
        product.visibility = action.payload.visibility
      }
      state.error = false;
    }
  }
})

export const {
  getProductsSuccess,
  addProductSuccess,
  deleteProductSuccess,
  toggleProductVisibilitySuccess,
  deleteProductFail
} = productsStore.actions;



