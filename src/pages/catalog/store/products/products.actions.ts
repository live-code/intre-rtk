import { AppThunk } from '../../../../App';
import axios from 'axios';
import { Product } from '../../model/product';
import { addProductSuccess, deleteProductFail, deleteProductSuccess, getProductsSuccess, toggleProductVisibilitySuccess } from './products.store';
import { setHttpStatus } from '../../../../core/store/http-status.store';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const getProducts3 = function(): AppThunk {
  return function (dispatch) {
    // ....
    axios.get<Product[]>('http://localhost:3001/products')
      .then(res => {
        dispatch(getProductsSuccess(res.data))
      })
  }
}


export const getProducts2 = (): AppThunk => dispatch => {
  axios.get<Product[]>('http://localhost:3001/products')
    .then(res => {
      dispatch(getProductsSuccess(res.data))
    })
}

// GET
export const getProducts = (): AppThunk => async dispatch => {
  const response = await axios.get<Product[]>('http://localhost:3001/products')
  dispatch(getProductsSuccess(response.data))
}

// DELETE
export const deleteProduct = (id: number): AppThunk => async dispatch => {
  try {
    const response = await axios.delete('http://localhost:3001/products/' + id)
    dispatch(deleteProductSuccess(id))
  } catch (e) {
    dispatch(deleteProductFail())
    dispatch(setHttpStatus({ status: 'error', actionType: 'deleteProduct' }))
  }

}


// ADD
export const addProductOldClassicThunk = (
  product: Partial<Product>
): AppThunk => async dispatch => {

  const response = await axios.post<Product>('http://localhost:3001/products/', {
    ...product,
    visibility: false,
  })
  dispatch(addProductSuccess(response.data))
}

export const addProduct = createAsyncThunk<Product, Partial<Product>>(
  'products/add',
  async (payload, {  dispatch, rejectWithValue })  => {
    try {
      const newProduct = await axios.post<Product>(
        'http://localhost:3001/products',
        { ...payload, visibility: false }
      );
      dispatch(addProductSuccess(newProduct.data))
      return newProduct.data;
    } catch (err) {
      // dispatch(setHttpStatus({ status: 'error', actionType: 'addProduct' }))
      return rejectWithValue('add Product fails!')
    }
  }
)


// TOGGLE
export const toggleProduct = (
  id: number,
  visibility: boolean
): AppThunk => async (dispatch) => {
  const response = await axios.patch<Product>(`http://localhost:3001/products/${id}`, {
    visibility
  })
  dispatch(toggleProductVisibilitySuccess(response.data))
}
