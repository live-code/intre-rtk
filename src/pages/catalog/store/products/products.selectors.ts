import { RootState } from '../../../../App';

export const getCatalogList = (state: RootState) => state.catalog.list;

export const getCatalogListByCat =
  (state: RootState) => state.catalog.categories.active === -1 ?
    state.catalog.list.data :
    state.catalog.list.data.filter(p => p.categoryId === state.catalog.categories.active)

export const getCatalogError = (state: RootState) => state.catalog.list.error;
