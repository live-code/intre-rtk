import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Product } from './model/product';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/products/products.actions';
import { ProductsList } from './components/ProductsList';
import { ProductsForm } from './components/ProductsForm';
import { getCatalogError, getCatalogListByCat } from './store/products/products.selectors';
import { getActiveCategory, getCategories } from './store/categories/categories.selectors';
import { changeActiveCategory } from './store/categories/categories.store';
import cn from 'classnames';
import { selectHttpStatus } from '../../core/store/http-status.store';
import {  AppDispatch } from '../../App';

export const CatalogPage: React.FC = () => {
  const list = useSelector(getCatalogListByCat)
  const categories = useSelector(getCategories)
  const activeCat = useSelector(getActiveCategory);
  const error = useSelector(getCatalogError);
  const errorStatus = useSelector(selectHttpStatus);
  const dispatch = useDispatch() as AppDispatch;

  useEffect(() => {
    dispatch(getProducts())
  }, []);

  function addProductHandler(product: Partial<Product>) {
    dispatch(addProduct(product))
      .then(res => console.log(res))
      .catch(err => console.log('errore:', err))
  }

  const onDeleteHandler = useCallback((id: number) => {
    dispatch(deleteProduct(id))
  }, []);

  const onToggle = useCallback((id: number, visibility: boolean) => {
    dispatch(toggleProduct(id, visibility))
  }, []);

  console.log('render: catalog page')
  return (
    <div>
      { error && <div className="alert alert-danger">Error</div> }
      {
        (errorStatus?.status === 'error' && errorStatus.actionType === 'deleteProduct')
          &&
          <div className="alert alert-danger">Cancellazione non avvenuta</div>
      }


      <ProductsForm
        onSubmit={addProductHandler}
        categories={categories}
      />
      <hr/>
      <hr/>
      <hr/>

      <button
        className={cn('btn btn-primary', { 'bg-dark': activeCat === -1})}
        onClick={() => dispatch(changeActiveCategory(-1))}>all</button>
      {
        categories.map(c => {
          return <button
            className={cn('btn btn-primary', { 'bg-dark': c.id === activeCat})}
            key={c.id}
            onClick={() => dispatch(changeActiveCategory(c.id))}
          >
            {c.name}
          </button>
        })
      }

      <ProductsList
        products={list}
        onDelete={onDeleteHandler}
        onToggle={onToggle}
      />
    </div>
  )
};
