export interface Product {
  title: string;
  id: number;
  price: number;
  visibility: boolean;
  categoryId: number;
}
