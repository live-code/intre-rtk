import React  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment } from './store/counter/counter.actions';
import { getCounter, getTotalPallet } from './store/counter/counter.selectors';
import { setConfig } from './store/config/config.actions';
import { getConfigMaterial } from './store/config/config.selectors';

export const CounterPage: React.FC = () => {
  const dispatch = useDispatch();
  const counter = useSelector(getCounter);
  const totalPallets = useSelector(getTotalPallet)
  const material = useSelector(getConfigMaterial)

  console.log('render')
  return <div>
    <div>
      <div>Products: {counter}</div>
      <div>Pallet: {totalPallets} of {material}</div>
    </div>
    <button onClick={() => dispatch(increment(5))}>+</button>
    <button onClick={() => dispatch(decrement(15))}>-</button>
    <button onClick={() => dispatch(setConfig({ material: 'plastic'}))}>Plastic</button>
  </div>
};
