export interface Config {
  itemsPerPallet: number;
  material: string;
  color: string;
}
