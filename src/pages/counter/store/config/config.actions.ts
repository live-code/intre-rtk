import { createAction } from '@reduxjs/toolkit';
import { Config } from '../../model/config';

export const setConfig = createAction<Partial<Config>>('config/set')
