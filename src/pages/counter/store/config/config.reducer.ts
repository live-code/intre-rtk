import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { Config } from '../../model/config';
import { setConfig } from './config.actions';

const initialState: Config = {
  itemsPerPallet: 12,
  material: 'wood',
  color: 'red'
}
export const configReducer = createReducer<Config>(initialState, {
  [setConfig.type]: (state: Config, action: PayloadAction<Partial<Config>>) => {
    return { ...state, ...action.payload  }
  }
})


// setConfig({ material: 'plastic'})
// setConfig({ itemsPerPallet: 6})
