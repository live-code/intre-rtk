import { RootState } from '../../../../App';

export const getConfig = (state: RootState) => state.warehouse.config;
export const getConfigMaterial = (state: RootState) => state.warehouse.config.material;

