import { RootState } from '../../../../App';

export const getCounter = (state: RootState) => state.warehouse.counter

export const getItemPerPallet = (state: RootState) => state.warehouse.config.itemsPerPallet

export const getTotalPallet = (state: RootState) => {
  return Math.ceil(state.warehouse.counter / state.warehouse.config.itemsPerPallet);
}
